'''
Created on 16-Oct-2017

@author: prakashsingh
'''

def partition(arr, start, end):
    pivot = arr[end]
    p_index = start
    
    for i in range(start, end):
        if arr[i] <= pivot:
            temp = arr[i]
            arr[i] = arr[p_index]
            arr[p_index]= temp
            p_index +=1
    arr[end] =  arr[p_index]   
    arr[p_index] = pivot
    
    return p_index

def quick_sort(arr, start, end):
    
    if start < end:
        p_index = partition(arr,start, end)
        quick_sort(arr, start, p_index-1)
        quick_sort(arr, p_index+1, end)
    return 

if __name__ == '__main__':
    arr = [7,2,1,6,8,5,3,4]
    quick_sort(arr,0,7)
    print(arr)


    
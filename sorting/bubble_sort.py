'''
Created on 13-Nov-2017

@author: prakashsingh
'''


def bubble_sort(arr):
    i = 0
    while(i<len(arr)-1):
        j=0
        while(j<len(arr)-1-i):
            if arr[j]> arr[j+1]:
                temp = arr[j]
                arr[j] = arr[j+1]
                arr[j+1] = temp
            j +=1
        i += 1
    return arr
        
if __name__ == "__main__":
    arr = [14, 33, 27, 10, 35, 19, 42, 44]
    print(bubble_sort(arr))
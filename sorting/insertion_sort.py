'''
Created on 13-Nov-2017

@author: prakashsingh
'''



def insertion_sort(arr):
    i =1
    while(i<len(arr)):
        temp = arr[i]
        j= i-1
        
        while(j>=0 and temp < arr[j]):
            arr[j+1]= arr[j]
            j -=1
            
        arr[j+1]= temp
        i +=1
    return arr

if __name__ == "__main__":
    arr = [14, 33, 27, 10, 35, 19, 42, 44]
    print(insertion_sort(arr))
'''
Created on 16-Oct-2017

@author: prakashsingh
'''

def merge(arr,start, mid, end):
    
    l_arr = []
    r_arr = []
    
    for i in range(start, mid+1):
        l_arr.append(arr[i])
    for j in range(mid+1, end+1):
        r_arr.append(arr[j])
     
    i = 0
    j = 0
    k = start
    
    while i < len(l_arr) and j < len(r_arr):
        if l_arr[i] <= r_arr[j]:
            arr[k] = l_arr[i]
            i +=1
        else:
            arr[k] = r_arr[j]
            j +=1
        k +=1
    
    while i < len(l_arr):
        arr[k] = l_arr[i]
        i +=1
        k +=1
        
    while j < len(r_arr):
        arr[k] = r_arr[j]
        j +=1
        k +=1
        

def merge_sort(arr, start, end):
    if start < end:
        mid = int((start + end)/2)
        merge_sort(arr, start, mid)
        merge_sort(arr, mid+1, end)
        merge(arr, start, mid, end)

if __name__ == '__main__':
    arr = [7,2,1,6,8,5,3,4]
    merge_sort(arr,0,7)
    print(arr)

'''
Created on 13-Nov-2017

@author: prakashsingh
'''

def selection_sort(arr):
    i=0
    while(i<len(arr)-1):
        smallest = arr[i]
        pos = i
        j=i+1
        while(j<len(arr)):
            if smallest > arr[j]:
                smallest = arr[j]
                pos = j
            j += 1
        temp = arr[i]
        arr[i] = arr[pos]
        arr[pos] = temp
        i += 1
    return arr

if __name__ == "__main__":
    arr = [14, 33, 27, 10, 35, 19, 42, 44]
    print(selection_sort(arr))
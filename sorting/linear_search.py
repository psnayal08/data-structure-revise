'''
Created on 13-Nov-2017

@author: prakashsingh
'''

def linear_search(arr, val):
    
    i = 0
    while(i<len(arr)):
        if val == arr[i]:
            return i
        i += 1
    return 

if __name__ == "__main__":
    arr = [7,2,1,6,8,5,3,4]
    val = 89
    pos = linear_search(arr, val)
    if pos:
        print(str(val), "found at index", str(pos))
    else:
        print(str(val), "not found in the given array")

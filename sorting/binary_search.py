'''
Created on 13-Nov-2017

@author: prakashsingh
'''

def binary_search(arr, val):
    start = 0
    end = len(arr)-1
    
    while(start <= end):
        mid = int((start + end)/2)
        if val == arr[mid]:
            return mid
        elif val < arr[mid]:
            end = mid -1
        else:
            start = mid + 1
    return 

if __name__ == "__main__":
    arr = [1,2,3,4,5,6,7,8]
    val = 3
    pos = binary_search(arr, val)
    if pos:
        print(str(val), "found at index", str(pos))
    else:
        print(str(val), "not found in the given array")
    